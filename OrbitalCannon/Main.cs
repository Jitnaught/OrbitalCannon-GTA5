﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Windows.Forms;

#pragma warning disable CS0618 //Enable/DisableControl obsolete warning. Decompiled script for orbital cannon uses DisableControl so I'll use it here too...

namespace OrbitalCannon
{
    public class Main : Script
    {
        Keys toggleKey;

        Camera orbitalCam;
        int orbitalCannonCamScaleformMovie;

        FrontendSound backgroundLoopSound = new FrontendSound("background_loop", "dlc_xm_orbital_cannon_sounds"),
            cannonActiveSound = new FrontendSound("cannon_active", "dlc_xm_orbital_cannon_sounds"),
            cannonActivatingLoopSound = new FrontendSound("cannon_activating_loop", "dlc_xm_orbital_cannon_sounds"),
            cannonChargeFireLoopSound = new FrontendSound("cannon_charge_fire_loop", "dlc_xm_orbital_cannon_sounds"),
            panLoopSound = new FrontendSound("pan_loop", "dlc_xm_orbital_cannon_sounds"),
            zoomLoopSound = new FrontendSound("zoom_out_loop", "dlc_xm_orbital_cannon_sounds");

        bool cannonActive = false, charged = false;
        int zoomLevel = 0;

        public Main()
        {
            toggleKey = Settings.GetValue("KEYS", "TOGGLE_KEY", Keys.O);

            KeyDown += Main_KeyDown;
            Tick += Main_Tick;
        }

        private void Main_Tick(object sender, EventArgs e)
        {
            if (cannonActive)
            {
                processCannon();
            }
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == toggleKey)
            {
                if (cannonActive) stopCannon();
                else startCannon();
            }
        }

        private void chargeCannon()
        {
            Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_STATE");
            Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_INT, 3);
            Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);

            for (int i = 0; i < 3000; i++)
            {
                switch (i)
                {
                    case 1000:
                    case 2000:
                    case 3000:
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_COUNTDOWN");
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_INT, i);
                        Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);
                        break;
                }

                Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_CHARGING_LEVEL");
                Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_FLOAT, (float)i / 3000f);
                Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);

                Wait(1);
            }

            charged = true;
        }

        private void startCannon()
        {
            Player plr = Game.Player;
            Ped plrPed = plr.Character;

            if (!Game.IsPaused && plrPed.Exists() && plrPed.IsAlive && !Function.Call<bool>(Hash.IS_PLAYER_BEING_ARRESTED, plr))
            {
                cannonActive = true;

                Function.Call(Hash.DISPLAY_CASH, 0);

                Function.Call(Hash.REQUEST_STREAMED_TEXTURE_DICT, "helicopterhud", 0);

                plr.CanControlCharacter = false;

                Function.Call(Hash.SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME, new InputArgument(World.RenderingCamera.Position));

                Function.Call(Hash.REQUEST_SCRIPT_AUDIO_BANK, "DLC_CHRISTMAS2017/XM_ION_CANNON", 0, -1);

                cannonActiveSound.Play();

                Function.Call(Hash._0x7F4724035FDCA1DD, 2); //_DISABLE_INPUT_GROUP

                Game.DisableControl(0, GTA.Control.Attack);
                Game.DisableControl(0, GTA.Control.Attack2);
                Game.DisableControl(0, GTA.Control.MeleeAttackLight);
                Game.DisableControl(0, GTA.Control.MeleeAttackHeavy);
                Game.DisableControl(0, GTA.Control.MeleeAttackAlternate);
                Game.DisableControl(0, GTA.Control.MeleeAttack1);
                Game.DisableControl(0, GTA.Control.MeleeAttack2);
                Game.DisableControl(0, GTA.Control.MeleeBlock);
                Game.DisableControl(0, GTA.Control.FrontendPauseAlternate);

                Game.FadeScreenOut(500);
                Wait(500);

                Function.Call(Hash._START_SCREEN_EFFECT, "MP_OrbitalCannon", 0, 1);

                Function.Call(Hash.CLEAR_HELP, 1);

                Function.Call(Hash.SET_OVERRIDE_WEATHER, "Clear");
                Function.Call(Hash.CLEAR_TIMECYCLE_MODIFIER);

                cannonActivatingLoopSound.Play();

                Function.Call(Hash.START_AUDIO_SCENE, "dlc_xm_orbital_cannon_camera_active_scene");

                orbitalCannonCamScaleformMovie = Function.Call<int>(Hash.REQUEST_SCALEFORM_MOVIE, "ORBITAL_CANNON_CAM");

                for (int i = 0; i < 5000; i++)
                {
                    if (Function.Call<bool>(Hash.HAS_SCALEFORM_MOVIE_LOADED, orbitalCannonCamScaleformMovie)) break;
                    Wait(1);
                }

                if (Function.Call<bool>(Hash.HAS_SCALEFORM_MOVIE_LOADED, orbitalCannonCamScaleformMovie))
                {
                    Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_ZOOM_LEVEL");
                    Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_FLOAT, 0f);
                    Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);

                    //if (CAM::DOES_CAM_EXIST(uParam0->f_4))
                    //{
                    //    CAM::DESTROY_CAM(uParam0->f_4, 0);
                    //}

                    Vector3 camPos = new Vector3(-8.8511f, 6835.003f, 400f), camRot = new Vector3(-90f, 0, 0);

                    orbitalCam = World.CreateCamera(camPos, camRot, 100f);
                    World.RenderingCamera = orbitalCam; //runs .IsActive = true and RENDER_SCRIPT_CAMS(true, ...)

                    Wait(200);

                    //if (GAMEPLAY::IS_BIT_SET(uParam0->f_10, 1))
                    //{
                    //    STREAMING::NEW_LOAD_SCENE_STOP();
                    //}
                    //if ((zoomLevel20 == 0f && zoomLevel20.f_1 == 0f) && zoomLevel20.f_2 == 0f)
                    //{
                    //    STREAMING::NEW_LOAD_SCENE_START_SPHERE(zoomLevel23, 300f, 0);
                    //}
                    //else
                    //{
                    //    STREAMING::NEW_LOAD_SCENE_START(zoomLevel23, zoomLevel20, 300f, 0);
                    //}

                    Function.Call(Hash.NEW_LOAD_SCENE_START, camPos.X, camPos.Y, camPos.Z, camRot.X, camRot.Y, camRot.Z, 300f, 0);

                    Function.Call(Hash._SET_FOCUS_AREA, camPos.X, camPos.Y, camPos.Z, -90f, 0f, 0f);

                    for (int i = 0; i < 10000; i++)
                    {
                        if (Function.Call<bool>(Hash.IS_NEW_LOAD_SCENE_LOADED)) break;

                        Wait(1);
                    }

                    Function.Call(Hash.NEW_LOAD_SCENE_STOP);

                    backgroundLoopSound.Play();

                    Game.FadeScreenIn(500);
                    Wait(500);

                    chargeCannon();
                    
                    //instructional button stuff
                    //if (GAMEPLAY::IS_BIT_SET(uParam0->f_5, 13) || CONTROLS::_0x6CD79468A1E595C6(0))
                    //{
                    //    func_121(&(uParam0->f_23));
                    //    func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(0, 202, true), "HUD_INPUT3", &(uParam0->f_23), 0);
                    //    func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(0, 203, true), "ORB_CAN_RE", &(uParam0->f_23), 0);
                    //    if (CONTROLS::_IS_INPUT_DISABLED(2))
                    //    {
                    //        func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(2, iVar3, true), "ORB_CAN_ZOOMO", &(uParam0->f_23), 0);
                    //        func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(2, iVar2, true), "ORB_CAN_ZOOMI", &(uParam0->f_23), 0);
                    //    }
                    //    else
                    //    {
                    //        func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(2, iVar2, true), "ORB_CAN_ZOOM", &(uParam0->f_23), 0);
                    //    }
                    //    func_120(CONTROLS::GET_CONTROL_INSTRUCTIONAL_BUTTON(2, 24, true), "ORB_CAN_FIRE", &(uParam0->f_23), 0);
                    //    GAMEPLAY::CLEAR_BIT(&(uParam0->f_5), 13);
                    //}
                    //Var5 = { func_119() };
                    //func_118(&(uParam0->f_23), 1f);
                    //func_110(&(uParam0->f_21), &Var5, &(uParam0->f_23), func_117(&(uParam0->f_23))); //more instructional button stuff
                    //func_109(1);
                }
                else stopCannon();
            }
        }

        /// <summary>
        /// Changes the zoom level and returns the new FOV for that zoom level
        /// </summary>
        /// <param name="camFov">Original camera FOV</param>
        /// <param name="zoomedIn"></param>
        /// <param name="zoomedOut"></param>
        /// <returns>New FOV</returns>
        float zoom(float camFov, bool zoomedIn, bool zoomedOut) //func_56
        {
            float fVar0 = 125f;

            float fov = camFov;

            if (zoomedOut)
            {
                fov = (fov + (fVar0 * Function.Call<float>(Hash.TIMESTEP)));
                switch (zoomLevel)
                {
                    case 1:
                        if (fov > 100f)
                        {
                            fov = 100f;
                            zoomLevel = 0;
                        }
                        break;

                    case 2:
                        if (fov > 80f)
                        {
                            fov = 80f;
                            zoomLevel = 1;
                        }
                        break;

                    case 3:
                        if (fov > 60f)
                        {
                            fov = 60f;
                            zoomLevel = 2;
                        }
                        break;

                    case 4:
                        if (fov > 40f)
                        {
                            fov = 40f;
                            zoomLevel = 3;
                        }
                        break;
                }
            }
            else if (zoomedIn)
            {
                fov = (fov - (fVar0 * Function.Call<float>(Hash.TIMESTEP)));

                switch (zoomLevel)
                {
                    case 0:
                        if (fov < 80f)
                        {
                            fov = 80f;
                            zoomLevel = 1;
                        }
                        break;

                    case 1:
                        if (fov < 60f)
                        {
                            fov = 60f;
                            zoomLevel = 2;
                        }
                        break;

                    case 2:
                        if (fov < 40f)
                        {
                            fov = 40f;
                            zoomLevel = 3;
                        }
                        break;

                    case 3:
                        if (fov < 20f)
                        {
                            fov = 20f;
                            zoomLevel = 4;
                        }
                        break;
                }
            }

            return fov;
        }

        float getScaleformZoomLevel(bool zoomedOut) //func_60, bParam1
        {
            if (!zoomedOut)
            {
                switch (zoomLevel)
                {
                    case 0:
                        return 0.25f;

                    case 1:
                        return 0.5f;

                    case 2:
                        return 0.75f;

                    case 3:
                        return 1f;
                }
            }
            else
            {
                switch (zoomLevel)
                {
                    case 1:
                        return 0f;

                    case 2:
                        return 0.25f;

                    case 3:
                        return 0.5f;

                    case 4:
                        return 0.75f;
                }
            }
            return 0f;
        }

        float getOrbitalCamHeightForPos(Vector3 pos) //func_61
        {
            if (pos.Y >= 1000f && pos.Y <= 1700f && pos.X >= -700f && pos.X <= 1100f)
            {
                return 500f;
            }

            if (pos.Y >= 3600f && pos.Y <= 4100f && pos.X >= -1700f && pos.X <= -700f)
            {
                return 550f;
            }

            if (pos.Y >= 4880f && pos.Y <= 6150f && pos.X >= -500f && pos.X <= 1900f)
            {
                return 850f;
            }

            return 400f;
        }

        float func_62(int zoomLevel)
        {
            float fVar0 = 100f;

            switch (zoomLevel)
            {
                case 0:
                    fVar0 = 90f;
                    break;

                case 1:
                    fVar0 = 60f;
                    break;

                case 2:
                    fVar0 = 50f;
                    break;

                case 3:
                    fVar0 = 25f;
                    break;

                case 4:
                    fVar0 = 10f;
                    break;
            }

            return fVar0;
        }

        private void _GET_GROUND_Z_COORD_WITH_OFFSETS(Vector3 pos, ref float groundZ, ref Vector3 offset)
        {
            OutputArgument outGroundZ = new OutputArgument(), outOffset = new OutputArgument();
            Function.Call(Hash._0x8BDC7BFC57A81E76, pos.X, pos.Y, pos.Z, outGroundZ, outOffset); //_GET_GROUND_Z_COORD_WITH_OFFSETS
            groundZ = outGroundZ.GetResult<float>();
            offset = outOffset.GetResult<Vector3>();
        }

        private void processCannon()
        {
            Player plr = Game.Player;
            Ped plrPed = plr.Character;

            if (!Game.IsLoading && plrPed.Exists() && plrPed.IsAlive && !Function.Call<bool>(Hash.IS_PLAYER_BEING_ARRESTED, plr) && orbitalCam.Exists())
            {
                Function.Call(Hash.DISPLAY_AMMO_THIS_FRAME, 0);
                Function.Call(Hash._SHOW_WEAPON_WHEEL, 0);
                Function.Call(Hash.HIDE_HUD_AND_RADAR_THIS_FRAME);
                Function.Call(Hash.HIDE_HUD_COMPONENT_THIS_FRAME, 1);
                Function.Call(Hash.HIDE_HUD_COMPONENT_THIS_FRAME, 2);
                Function.Call(Hash.HIDE_HUD_COMPONENT_THIS_FRAME, 3);
                Function.Call(Hash.HIDE_HUD_COMPONENT_THIS_FRAME, 4);
                Function.Call(Hash.HIDE_HUD_COMPONENT_THIS_FRAME, 19);

                Function.Call(Hash._SET_2D_LAYER, 0);
                Function.Call(Hash.DRAW_SCALEFORM_MOVIE_FULLSCREEN, orbitalCannonCamScaleformMovie, 255, 255, 255, 0, 1);
                Function.Call(Hash._0xE3A3DB414A373DAB); //_SCREEN_DRAW_POSITION_END

                if (charged && Game.IsDisabledControlJustPressed(2, GTA.Control.Attack) && !Function.Call<bool>(Hash._0xE18B138FABC53103/*IS_WARNING_MESSAGE_ACTIVE*/) && orbitalCam.Exists()) //24 = INPUT_ATTACK
                {
                    cannonChargeFireLoopSound.Play();

                    Function.Call(Hash.SET_PAD_SHAKE, 0, 3000, 50);

                    Function.Call(Hash.SET_VARIABLE_ON_SOUND, cannonChargeFireLoopSound.SoundID, "Firing", 0f);

                    bool heldButton = true;

                    for (int i = 0; i < 3000; i++)
                    {
                        if (!Game.IsDisabledControlPressed(2, GTA.Control.Attack))
                        {
                            heldButton = false;
                            break;
                        }

                        Wait(1);
                    }

                    Function.Call(Hash.SET_VARIABLE_ON_SOUND, cannonChargeFireLoopSound.SoundID, "Firing", 1f);

                    if (heldButton)
                    {
                        Vector3 orbitalCamPos = orbitalCam.Position;

                        //Vector3 explosionPos = orbitalCamPos;
                        //explosionPos.Z = World.GetGroundHeight(orbitalCamPos);

                        float groundZ = 0f;
                        Vector3 offset = Vector3.Zero;
                        _GET_GROUND_Z_COORD_WITH_OFFSETS(orbitalCamPos, ref groundZ, ref offset);

                        Vector3 explosionPos = orbitalCamPos;
                        explosionPos.Z = groundZ;

                        World.AddOwnedExplosion(plrPed, explosionPos, (ExplosionType)59, 1f, 1065353216);

                        Function.Call(Hash._SET_PTFX_ASSET_NEXT_CALL, "scr_xm_orbital"); //_USE_PARTICLE_FX_ASSET_NEXT_CALL
                        Function.Call(Hash._START_PARTICLE_FX_NON_LOOPED_AT_COORD_2, "scr_xm_orbital_blast", orbitalCamPos.X, orbitalCamPos.Y, orbitalCamPos.Z, offset.X, offset.Y, offset.Z, 1065353216, 0, 0, 0, 1);
                        Audio.PlaySoundAt(explosionPos, "DLC_XM_Explosions_Orbital_Cannon");
                        Function.Call(Hash.SET_PAD_SHAKE, 0, 500, 256);
                        Function.Call(Hash.SHAKE_CAM, orbitalCam, "GAMEPLAY_EXPLOSION_SHAKE", 1.5f);

                        Wait(3000);

                        chargeCannon();
                    }
                }
                //else if (inactive)? else if (not charged yet)?
                //{
                //    AUDIO::PLAY_SOUND_FRONTEND(-1, "inactive_fire_fail", "dlc_xm_orbital_cannon_sounds", 1);
                //}

                if (Game.IsDisabledControlJustPressed(2, GTA.Control.FrontendCancel)) //202 = INPUT_FRONTEND_CANCEL
                {
                    Function.Call(Hash.SET_GAMEPLAY_CAM_RELATIVE_HEADING, 0f);
                    Function.Call(Hash.SET_GAMEPLAY_CAM_RELATIVE_PITCH, 0f, 1065353216);

                    stopCannon();

                    return;
                }

                if (Game.IsDisabledControlJustReleased(2, GTA.Control.FrontendX)) //203 = INPUT_FRONTEND_X
                {
                    if (orbitalCam.IsShaking) orbitalCam.StopShaking();

                    Function.Call(Hash.STOP_PAD_SHAKE, 0);
                }

                #region camera movement
                float fov = orbitalCam.FieldOfView;

                float leftAxisX = Game.GetControlNormal(2, GTA.Control.ScriptLeftAxisX);
                float leftAxisY = Game.GetControlNormal(2, GTA.Control.ScriptLeftAxisY);
                float rightAxisY = Game.GetControlNormal(2, GTA.Control.ScriptRightAxisY);

                float fVar4 = (35f + func_62(zoomLevel));

                Vector3 newCamPos = orbitalCam.Position;

                bool movedCam = false, zoomedOut = false, zoomedIn = false;

                if (leftAxisX > 0.1f)
                {
                    if ((newCamPos.X + (Math.Abs((fVar4 * leftAxisX)) * Function.Call<float>(Hash.TIMESTEP))) <= 4000f)
                    {
                        movedCam = true;
                        newCamPos.X = (newCamPos.X + (Math.Abs((fVar4 * leftAxisX)) * Function.Call<float>(Hash.TIMESTEP)));
                    }
                }
                else if (leftAxisX < -0.1f)
                {
                    if ((newCamPos.X - (Math.Abs((fVar4 * leftAxisX)) * Function.Call<float>(Hash.TIMESTEP))) >= -4000f)
                    {
                        movedCam = true;
                        newCamPos.X = (newCamPos.X - (Math.Abs((fVar4 * leftAxisX)) * Function.Call<float>(Hash.TIMESTEP)));
                    }
                }
                if (leftAxisY > 0.1f)
                {
                    if ((newCamPos.Y - (Math.Abs((fVar4 * leftAxisY)) * Function.Call<float>(Hash.TIMESTEP))) >= -4000f)
                    {
                        movedCam = true;
                        newCamPos.Y = (newCamPos.Y - (Math.Abs((fVar4 * leftAxisY)) * Function.Call<float>(Hash.TIMESTEP)));
                    }
                }
                else if (leftAxisY < -0.1f)
                {
                    if ((newCamPos.Y + (Math.Abs((fVar4 * leftAxisY)) * Function.Call<float>(Hash.TIMESTEP))) <= 8000f)
                    {
                        movedCam = true;
                        newCamPos.Y = (newCamPos.Y + (Math.Abs((fVar4 * leftAxisY)) * Function.Call<float>(Hash.TIMESTEP)));
                    }
                }

                float camHeightForNewPos = getOrbitalCamHeightForPos(newCamPos);

                if (newCamPos.Z != camHeightForNewPos)
                {
                    if (newCamPos.Z < camHeightForNewPos)
                    {
                        //smoothly move camera upwards
                        newCamPos.Z = (newCamPos.Z + (Math.Abs(fVar4) * Function.Call<float>(Hash.TIMESTEP)));
                        if (newCamPos.Z > camHeightForNewPos)
                        {
                            newCamPos.Z = camHeightForNewPos;
                        }
                    }
                    else if (newCamPos.Z > camHeightForNewPos)
                    {
                        //smoothly move cam downwards
                        newCamPos.Z = (newCamPos.Z - (Math.Abs(fVar4) * Function.Call<float>(Hash.TIMESTEP)));
                        if (newCamPos.Z < camHeightForNewPos)
                        {
                            newCamPos.Z = camHeightForNewPos;
                        }
                    }
                    movedCam = true;
                }

                //7 == zoom out, 6 == zoom in
                if ((!Function.Call<bool>(Hash._GET_LAST_INPUT_METHOD/*_IS_INPUT_DISABLED*/, 2) && rightAxisY > 0.3f) || 
                    Game.IsDisabledControlJustPressed(2, GTA.Control.CursorScrollDown))
                {
                    if (!zoomedOut && !zoomedIn && zoomLevel > 0)
                    {
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_ZOOM_LEVEL");
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_FLOAT, getScaleformZoomLevel(true));
                        Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);
                        zoomedOut = true;
                    }
                }
                else if ((!Function.Call<bool>(Hash._GET_LAST_INPUT_METHOD/*_IS_INPUT_DISABLED*/, 2) && rightAxisY < -0.3f) || 
                    Game.IsDisabledControlJustPressed(2, GTA.Control.CursorScrollUp))
                {
                    if (!zoomedIn && !zoomedOut && zoomLevel < 4)
                    {
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION, orbitalCannonCamScaleformMovie, "SET_ZOOM_LEVEL");
                        Function.Call(Hash._PUSH_SCALEFORM_MOVIE_FUNCTION_PARAMETER_FLOAT, getScaleformZoomLevel(false));
                        Function.Call(Hash._POP_SCALEFORM_MOVIE_FUNCTION_VOID);
                        zoomedIn = true;
                    }
                }

                if (movedCam)
                {
                    panLoopSound.Play();
                    zoomLoopSound.Play();
                }
                else
                {
                    panLoopSound.Stop();
                    zoomLoopSound.Stop();
                }

                float newFov = zoom(orbitalCam.FieldOfView, zoomedIn, zoomedOut);

                zoomedOut = false;
                zoomedIn = false;

                orbitalCam.FieldOfView = newFov;
                orbitalCam.Position = newCamPos;

                float fVar9 = 0f;
                float fVar10 = 0f;
                Vector3 origCamPos = orbitalCam.Position;

                if (origCamPos.X < newCamPos.X)
                {
                    fVar9 = 50f;
                }
                else if (origCamPos.X > newCamPos.X)
                {
                    fVar9 = -50f;
                }
                if (origCamPos.Y < newCamPos.Y)
                {
                    fVar10 = 50f;
                }
                else if (origCamPos.Y > newCamPos.Y)
                {
                    fVar10 = -50f;
                }

                OutputArgument out_fVar12 = new OutputArgument();

                Function.Call((Hash)0x9E82F0F362881B29, new InputArgument(newCamPos), out_fVar12, 1, 0); //Any _0x9E82F0F362881B29(Any p0, Any p1, Any p2, Any p3, Any p4) // 0x9E82F0F362881B29 b505
                Function.Call(Hash._SET_FOCUS_AREA, (newCamPos.X + fVar9), (newCamPos.Y + fVar10), (out_fVar12.GetResult<float>() + 50f), -90f, 0f, 0f);
                #endregion
            }
            else stopCannon();
        }

        private void stopCannon()
        {
            cannonActive = false;
            charged = false;

            Function.Call(Hash.SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED, "helicopterhud");

            backgroundLoopSound.Stop();
            cannonActiveSound.Stop();
            cannonActivatingLoopSound.Stop();
            cannonChargeFireLoopSound.Stop();
            panLoopSound.Stop();
            zoomLoopSound.Stop();
            
            Function.Call(Hash.RELEASE_NAMED_SCRIPT_AUDIO_BANK, "DLC_CHRISTMAS2017/XM_ION_CANNON");
            Function.Call(Hash.STOP_AUDIO_SCENE, "dlc_xm_orbital_cannon_camera_active_scene");

            if (Function.Call<bool>(Hash._GET_SCREEN_EFFECT_IS_ACTIVE, "MP_OrbitalCannon"))
                Function.Call(Hash._STOP_SCREEN_EFFECT, "MP_OrbitalCannon");

            Function.Call(Hash.CLEAR_OVERRIDE_WEATHER);

            Function.Call(Hash.DISPLAY_CASH, 1);

            if (orbitalCam.Exists())
            {
                World.RenderingCamera = null; //reset cam back to default gameplay cam
                orbitalCam.Destroy();
            }

            if (Function.Call<bool>(Hash.HAS_SCALEFORM_MOVIE_LOADED, orbitalCannonCamScaleformMovie))
                Function.Call(Hash.SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED, orbitalCannonCamScaleformMovie);

            if (Game.IsScreenFadedOut || Game.IsScreenFadingOut) Game.FadeScreenIn(500);

            Function.Call(Hash.CLEAR_FOCUS);

            Game.EnableControl(0, GTA.Control.Attack);
            Game.EnableControl(0, GTA.Control.Attack2);
            Game.EnableControl(0, GTA.Control.MeleeAttackLight);
            Game.EnableControl(0, GTA.Control.MeleeAttackHeavy);
            Game.EnableControl(0, GTA.Control.MeleeAttackAlternate);
            Game.EnableControl(0, GTA.Control.MeleeAttack1);
            Game.EnableControl(0, GTA.Control.MeleeAttack2);
            Game.EnableControl(0, GTA.Control.MeleeBlock);
            Game.EnableControl(0, GTA.Control.FrontendPauseAlternate);

            Game.Player.CanControlCharacter = true;
        }
    }
}
