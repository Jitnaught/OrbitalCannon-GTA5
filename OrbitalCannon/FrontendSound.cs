﻿using GTA;

namespace OrbitalCannon
{
    class FrontendSound
    {
        public int SoundID = -1;
        public string SoundName, SoundSet;

        public FrontendSound(string sound, string set)
        {
            SoundName = sound;
            SoundSet = set;
        }

        public void Play(bool playIfAlreadyPlaying = false)
        {
            if (playIfAlreadyPlaying || SoundID != -1)
            {
                Stop();
                Audio.PlaySoundFrontend(SoundName, SoundSet);
            }
        }

        public void Stop()
        {
            if (SoundID != -1)
            {
                Audio.StopSound(SoundID);
                Audio.ReleaseSound(SoundID);
                SoundID = -1;
            }
        }
    }
}
